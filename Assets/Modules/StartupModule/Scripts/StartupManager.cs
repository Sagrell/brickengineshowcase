using BrickEngine;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine;

#if UNITY_EDITOR
#endif

namespace StartupModule
{
	public class StartupManager : MonoBehaviour
	{
		[SerializeField]
		private LoadingScreenView _loadingScreenView;
		[SerializeField]
		private StartupSettings _startupSettings;

		private LoadingScreenController _loadingScreen;
		private IStartupController      _startupController;
		
		private void Awake()
		{
			Application.targetFrameRate = 60;

			EnvironmentsManager.Init(true);

			GameController.OnGameReload += LoadGame;
			GameController.OnSwitchEnvironment += SwitchEnvironment;
			
			_loadingScreen = new LoadingScreenController(_loadingScreenView);
			
			_startupController = new GameStartupController(_loadingScreen, _startupSettings.GameConfig);
			
			LoadGame();
		}
		
		private void SwitchEnvironment(EEnvironmentType environmentType)
		{
			SwitchEnvironmentAsync(environmentType).Forget();
		}

		private async UniTaskVoid SwitchEnvironmentAsync(EEnvironmentType environmentType)
		{
			_loadingScreen.StartLoading();

			ProgressHandler progressHandler = new ProgressHandler();
			progressHandler.OnProgressChanged += _loadingScreen.ReportProgress;

			ProgressNode progress = progressHandler.CreateNode();
			await EnvironmentsManager.LoadEnvironment(environmentType, progress);
			
			_loadingScreen.StopLoading();
		}
		
		private void LoadGame()
		{
			_startupController.Load();
		}
	}
}