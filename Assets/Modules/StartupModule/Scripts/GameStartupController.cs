using BrickEngine;
using Cysharp.Threading.Tasks;

namespace StartupModule
{
	public class GameStartupController : IStartupController
	{
		private readonly LoadingScreenController _loadingScreen;
		private readonly StartupConfig _currentConfig;
		public GameStartupController(LoadingScreenController loadingScreen, StartupConfig config)
		{
			_loadingScreen = loadingScreen;
			_currentConfig = config;
			EnvironmentsManager.RegisterConfig(_currentConfig);
		}

		public void Load()
		{
			LoadAsync().Forget();
		}

		private async UniTaskVoid LoadAsync()
		{
			_loadingScreen.StartLoading();

			ProgressHandler progressHandler = new ProgressHandler();
			progressHandler.OnProgressChanged += _loadingScreen.ReportProgress;

			ProgressNode root = progressHandler.CreateNode();
			ProgressNode shelterProgress = root.CreateChild();
			if (EnvironmentsManager.HasLoadedEnvironments)
			{
				ProgressNode unloadProgress = root.CreateChild();
				await EnvironmentsManager.UnloadAllEnvironments(unloadProgress);
			}
			await EnvironmentsManager.LoadEnvironment(_currentConfig.StartEnvironment, shelterProgress);

			_loadingScreen.StopLoading();
		}
	}
}