﻿using BrickEngine;
using UnityEngine;

namespace StartupModule
{
    [CreateAssetMenu(fileName = "StartupSettings", menuName = "Settings/StartupSettings")]
    public class StartupSettings : ScriptableObject
    {
        public StartupConfig GameConfig;
    }
}