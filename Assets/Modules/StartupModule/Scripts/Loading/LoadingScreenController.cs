﻿using BrickEngine;

namespace StartupModule
{
	public class LoadingScreenController
	{
		private readonly LoadingScreenView _view;
		public LoadingScreenController(LoadingScreenView view)
		{
			_view = view;
		}
		
		public void StartLoading()
		{
			SetActive(true);
			_view.SetProgress(0);
		}
		
		public void StopLoading()
		{
			SetActive(false);
		}

		public void ReportProgress(ProgressData progress)
		{
			_view.SetProgress(progress.Value);
		}

		private void SetActive(bool active)
		{
			_view.gameObject.SetActive(active);
		}
	}
}