using UnityEngine;
using UnityEngine.UI;

namespace StartupModule
{
	public class LoadingScreenView : MonoBehaviour
	{
		[SerializeField]
		private Slider _slider;
		
		public void SetProgress(float value)
		{
			_slider.value = value;
		}
	}
}