﻿using System;

namespace UnityTools
{
	public interface IEventManager
	{
		IDisposable Subscribe(EUnityEvent eventType, Action action);
	}
}