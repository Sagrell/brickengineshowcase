﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityTools
{
    public enum EUnityEvent
    {
        Update,
        FixedUpdate,
        Gizmos,
        UpdateEachSecond,
        LateUpdate
    }
    
    internal sealed class UnityEventManager : IEventManager
    {
        private readonly UnityEventHolder _eventHolder;
        public UnityEventManager()
        {
            _eventHolder = new GameObject("UnityEventHolder").AddComponent<UnityEventHolder>();
        }

        public IDisposable Subscribe(EUnityEvent eventType, Action action)
        {
            switch (eventType)
            {
                case EUnityEvent.Update:
                    _eventHolder.OnUpdate += action;
                    break;
                case EUnityEvent.FixedUpdate:
                    _eventHolder.OnFixedUpdate += action;
                    break;
                case EUnityEvent.Gizmos:
                    _eventHolder.OnGizmos += action;
                    break;
                case EUnityEvent.UpdateEachSecond:
                    _eventHolder.OnEachSecondUpdate += action;
                    break;
                case EUnityEvent.LateUpdate:
                    _eventHolder.OnLateUpdate += action;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(eventType), eventType, null);
            }

            return new DisposableActionHolder(() =>
            {
                Unsubscribe(eventType, action);
            });
        }

        private void Unsubscribe(EUnityEvent eventType, Action action)
        {
            switch (eventType)
            {
                case EUnityEvent.Update:
                    _eventHolder.OnUpdate -= action;
                    break;
                case EUnityEvent.FixedUpdate:
                    _eventHolder.OnFixedUpdate -= action;
                    break;
                case EUnityEvent.Gizmos:
                    _eventHolder.OnGizmos -= action;
                    break;
                case EUnityEvent.UpdateEachSecond:
                    _eventHolder.OnEachSecondUpdate -= action;
                    break;
                case EUnityEvent.LateUpdate:
                    _eventHolder.OnLateUpdate -= action;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(eventType), eventType, null);
            }
        }
    }
}