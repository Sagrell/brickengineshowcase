﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityTools
{
    public sealed class UpdateTimerManager : IDisposable
    {
        private readonly IDisposable _updateDisposable;
        public UpdateTimerManager(IEventManager unityEventManager)
        {
            _updateDisposable = unityEventManager.Subscribe(EUnityEvent.Update, Update);
        }

        public void Dispose()
        {
            _updateDisposable.Dispose();
            _timers.Clear();
        }
        

        private readonly List<IUpdateTimer> _timers = new List<IUpdateTimer>();

        public CountdownTimer CreateCountdownTimer(CountdownTimerSettings countdownTimerSettings, Action onComplete, Action onEachSecond = null)
        {
            CountdownTimer countdownTimer = new CountdownTimer(countdownTimerSettings, onComplete, onEachSecond);
            _timers.Add(countdownTimer);
            return countdownTimer;
        }

        public void DisposeTimer(CountdownTimer timer)
        {
            _timers.Remove(timer);
        }
        
        private void Update()
        {
            float deltaTime = Time.deltaTime;
            for (int i = 0; i < _timers.Count; i++)
            {
                _timers[i].Update(deltaTime);
            }
        }
    }
}