using BrickEngine;

namespace UnityTools
{
	public sealed class ToolsModuleInitializer : ModuleInitializer
	{
		public override void Initialize()
		{
			Register<IEventManager, UnityEventManager>();
			Register<UpdateTimerManager>();
		}
	}
}