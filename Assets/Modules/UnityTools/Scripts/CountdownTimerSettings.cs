namespace UnityTools
{
    public readonly struct CountdownTimerSettings
    {
        public readonly float Duration;
        public readonly bool Repeat;

        public CountdownTimerSettings(float duration, bool repeat)
        {
            Duration = duration;
            Repeat = repeat;
        }
    }
}