﻿using System;
using UnityEngine;

namespace UnityTools
{
    internal sealed class UnityEventHolder : MonoBehaviour
    {
        public event Action OnUpdate;
        public event Action OnLateUpdate;
        public event Action OnEachSecondUpdate;
        public event Action OnFixedUpdate;
        public event Action OnGizmos;
        
        private float _secondElapsedTime;
        private void Update()
        {
            OnUpdate?.Invoke();
            _secondElapsedTime += Time.deltaTime;
            while (_secondElapsedTime >= 1f)
            {
                _secondElapsedTime--;
                OnEachSecondUpdate?.Invoke();
            }
        }

        private void LateUpdate()
        {
            OnLateUpdate?.Invoke();
        }

        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }
        
        private void OnDrawGizmos()
        {
            OnGizmos?.Invoke();
        }
    }
}