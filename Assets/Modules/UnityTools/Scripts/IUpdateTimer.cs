﻿namespace UnityTools
{
    public interface IUpdateTimer
    {
        void Update(float deltaTime);
    }
}