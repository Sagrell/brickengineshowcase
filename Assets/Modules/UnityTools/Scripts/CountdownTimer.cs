﻿using System;

namespace UnityTools
{
    public sealed class CountdownTimer : IUpdateTimer
    {
        private readonly CountdownTimerSettings _settings;
        private readonly Action _onComplete;
        private readonly Action _onEachSecond;
        private readonly bool _eachSecond;

        private float _elapsedSeconds;
        private float _elapsedTime;
        private float _elapsedEachSeconds;

        private bool _completed;
        private bool _active;

        public CountdownTimer(CountdownTimerSettings settings, Action onComplete, Action onEachSecond = null)
        {
            _settings = settings;
            _onComplete = onComplete;
            _completed = false;
            _eachSecond = onEachSecond != null;
            _onEachSecond = onEachSecond;
        }

        public void Reset()
        {
            _elapsedSeconds = 0;
            _elapsedEachSeconds = 0;
            _completed = false;
            _active = false;
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = false;
        }

        public void Update(float deltaTime)
        {
            if (!_active || _completed) return;

            _elapsedSeconds += deltaTime;
            if (_elapsedSeconds >= _settings.Duration)
            {
                _elapsedSeconds = 0;
                _onComplete();
                if (!_settings.Repeat)
                {
                    _completed = true;
                }
            }

            if (!_eachSecond) return;

            _elapsedEachSeconds += deltaTime;
            if (_elapsedEachSeconds >= 1)
            {
                _elapsedEachSeconds = 0;
                _onEachSecond();
            }
        }
    }
}