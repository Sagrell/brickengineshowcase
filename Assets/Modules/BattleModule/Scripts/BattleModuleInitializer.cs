using BrickEngine;

namespace BattleModule
{
	public class BattleModuleInitializer : ModuleInitializer
	{
		public override void Initialize()
		{
			Register<BattleRuntimeController>();
			Register<IBattleDataProvider, BattleDataProvider>();
		}
	}
}