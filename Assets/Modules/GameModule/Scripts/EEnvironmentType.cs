﻿namespace GameModule
{
    public enum EEnvironmentType
    {
        Core = 1,
        Shelter = 2,
        Battle = 3,
        Editor = 10
    }
}