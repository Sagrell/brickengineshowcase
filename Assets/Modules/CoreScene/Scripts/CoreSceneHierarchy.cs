﻿using UnityEngine;

namespace CoreScene
{
	public class CoreSceneHierarchy : MonoBehaviour
	{
		public Transform WindowHolder;
		public Camera    UICamera;
		public Transform HudHolder;
		public Transform OverlayHolder;
	}
}