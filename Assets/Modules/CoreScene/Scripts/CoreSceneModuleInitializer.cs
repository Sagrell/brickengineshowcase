﻿using BrickEngine;

namespace CoreScene
{
    public class CoreSceneModuleInitializer : ModuleInitializer
    {
        public override void Initialize()
        {
            Register<CoreSceneController>();
        }
    }
}