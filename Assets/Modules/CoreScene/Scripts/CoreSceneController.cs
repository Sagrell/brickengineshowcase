﻿using UnityEngine;

namespace CoreScene
{
    public class CoreSceneController
    {
        public readonly CoreSceneHierarchy SceneHierarchy;
        public CoreSceneController()
        {
            SceneHierarchy = Object.FindObjectOfType<CoreSceneHierarchy>();
        }
    }
}