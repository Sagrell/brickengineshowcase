using BattleModule;
using BrickEngine;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine.SceneManagement;

namespace BattleEnvironment
{
	public class BattleEnvironment : BrickEnvironment
	{
		private const int SceneIndex = 3;
		public override EEnvironmentType EnvironmentType => EEnvironmentType.Battle;
		protected override void FillModules()
		{
			Add<BattleModuleInitializer>();
		}

		protected override async UniTask Load(ProgressNode progressNode = null)
		{
			await LoadScene(SceneIndex, LoadSceneMode.Additive);
		}


		protected override void OnEnvironmentReady()
		{
		}
	}
}