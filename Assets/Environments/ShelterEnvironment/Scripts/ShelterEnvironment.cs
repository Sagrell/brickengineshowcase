using BrickEngine;
using Cysharp.Threading.Tasks;
using GameModule;
using ShelterModule;
using UnityEngine.SceneManagement;

namespace ShelterEnvironment
{
    public class ShelterEnvironment : BrickEnvironment
    {
        public const int ShelterSceneIndex = 2;
        public override EEnvironmentType EnvironmentType => EEnvironmentType.Shelter;

        protected override void FillModules()
        {
            Add<ShelterModuleInitializer>();
        }

        protected override async UniTask Load(ProgressNode progressNode = null)
        {
            await LoadScene(ShelterSceneIndex, LoadSceneMode.Additive);
        }

        protected override void OnEnvironmentReady()
        {
        }
    }
}