﻿using BrickEngine;
using CoreScene;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine.SceneManagement;
using UnityTools;

namespace CoreEnvironment
{
	public class CoreEnvironment : BrickEnvironment
	{
		private const int CoreSceneIndex = 1;

		public override EEnvironmentType EnvironmentType => EEnvironmentType.Core;

		protected override void FillModules()
		{
			Add<CoreSceneModuleInitializer>();
			Add<ToolsModuleInitializer>();
		}

		protected override async UniTask Load(ProgressNode progressNode = null)
		{
			ProgressNode progress = progressNode?.CreateChild("Loading core scene");
			progress?.SetTitle();
			await LoadScene(CoreSceneIndex, LoadSceneMode.Additive);
			progress?.CompleteAll();
		}
	}
}