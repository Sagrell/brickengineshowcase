﻿using BrickEngine;
using UnityTools;

namespace EditorEnvironment
{
	public class EditorModuleInitializer : ModuleInitializer
	{
		public override void Initialize()
		{
			Register<IEventManager, EditorEventManager>();
		}
	}
}