﻿using System;
using UnityTools;

namespace EditorEnvironment
{
	public class EditorEventManager : IEventManager
	{
		public IDisposable Subscribe(EUnityEvent eventType, Action action)
		{
			return new DisposableActionHolder(() => { });
		}
	}
}