﻿using System;
using System.Collections.Generic;
using BrickEngine;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine;
using Object = UnityEngine.Object;

namespace EditorEnvironment
{
	public class EditorEnvironmentContext : IDisposable
	{
		private StartupConfig _config;
		public async UniTask Load()
		{
			Debug.Log("Loading editor environment...");
			EnvironmentsManager.Init(false);
			_config = ScriptableObject.CreateInstance<StartupConfig>();
			_config.StartEnvironment = EEnvironmentType.Editor;
			_config.Environments = new List<StartupEnvironmentConfig>()
			{
				new StartupEnvironmentConfig()
				{
					Type = EEnvironmentType.Editor,
					Required = new List<EEnvironmentType>()
				}
			};
			EnvironmentsManager.RegisterConfig(_config);
			await EnvironmentsManager.LoadEnvironment(EEnvironmentType.Editor);
			Debug.Log("Editor environment loaded");
		}
		
		public async void Dispose()
		{	
			Debug.Log("Disposing editor environment...");
			await EnvironmentsManager.UnloadEnvironment(EEnvironmentType.Editor);
			Object.DestroyImmediate(_config);
			Debug.Log("Editor environment disposed");
		}
		
		public void Get<T>(out T dependency)
		{
			dependency = Get<T>();
		}

		public T Get<T>()
		{
			return EnvironmentsManager.GlobalContainerDebug.Resolve<T>();
		}
	}
	
	public static class EditorEnvironmentContextFabric
	{
		public static async UniTask<EditorEnvironmentContext> Create()
		{
			var context = new EditorEnvironmentContext();
			await context.Load();
			return context;
		}
	}
}