﻿using BrickEngine;
using GameModule;

namespace EditorEnvironment
{
	public class EditorEnvironment : BrickEnvironment
	{
		public override EEnvironmentType  EnvironmentType => EEnvironmentType.Editor;
		protected override void FillModules()
		{
			Add<EditorModuleInitializer>();
		}
	}
}