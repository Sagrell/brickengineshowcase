﻿using UnityEngine;

namespace BrickEngine.Editor
{
    public class DependencyNode
    {
        public Rect Rect;
        public string Title;
 
        public GUIStyle style;
        
        
        public ConnectionPoint inPoint;
        public ConnectionPoint outPoint;
 
        public DependencyNode(string title, Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle inPointStyle, GUIStyle outPointStyle)
        {
            Title = title;
            Rect = new Rect(position.x, position.y, width, height);
            style = nodeStyle;
            inPoint = new ConnectionPoint(this, ConnectionPointType.In, inPointStyle);
            outPoint = new ConnectionPoint(this, ConnectionPointType.Out, outPointStyle);
        }
 
        public void Drag(Vector2 delta)
        {
            Rect.position += delta;
        }
 
        public void Draw()
        {
            inPoint.Draw();
            outPoint.Draw();
            GUI.Box(Rect, Title, style);
        }
 
        public bool ProcessEvents(Event e)
        {
            return false;
        }
    }

}