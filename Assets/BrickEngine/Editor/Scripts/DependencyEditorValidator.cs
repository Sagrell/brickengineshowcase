﻿using UnityEditor;
using UnityEditor.Callbacks;

namespace BrickEngine.Editor.Scripts
{
    public static class DependencyEditorValidator
    {
        [MenuItem("BrickEngine/Validate")]
        [DidReloadScripts]
        public static void Validate()
        {
            StartupConfig config = StartupSettingsEditorHolder.Settings.GameConfig;
            config.Validate(false);
        }
        
        [MenuItem("BrickEngine/Validate with collections")]
        public static void ValidateWithCollections()
        {
            StartupConfig config = StartupSettingsEditorHolder.Settings.GameConfig;
            config.Validate(true);
        }
    }
}