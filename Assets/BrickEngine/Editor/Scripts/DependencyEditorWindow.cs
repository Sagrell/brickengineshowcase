﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BrickEngine.Editor
{
    public class DependencyEditorWindow : EditorWindow
    {
        [MenuItem("BrickEngine/DependencyWindow")]
        private static void OpenWindow()
        {
            CreateStyles();
            FillNodes();
            DependencyEditorWindow window = GetWindow<DependencyEditorWindow>();
            window.titleContent = new GUIContent("Node Based Editor");
        }
        
        private static GUIStyle _inPointStyle;
        private static GUIStyle _outPointStyle;
        private static GUIStyle _nodeStyle;
        
        private static void CreateStyles()
        {
            _nodeStyle = new GUIStyle();
            _nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            _nodeStyle.border = new RectOffset(12, 12, 12, 12);
            _nodeStyle.fontSize = 20;
            _nodeStyle.fontStyle = FontStyle.Bold;
            _nodeStyle.normal.textColor = Color.white;
            _nodeStyle.alignment = TextAnchor.MiddleCenter;
 
            _inPointStyle = new GUIStyle();
            _inPointStyle.normal.background = EditorGUIUtility.Load("Builtin Skins/DarkSkin/Images/ArrowNavigationLeft.png") as Texture2D;
            _inPointStyle.active.background = EditorGUIUtility.Load("Builtin Skins/DarkSkin/Images/ArrowNavigationLeft.png") as Texture2D;
 
            _outPointStyle = new GUIStyle();
            _outPointStyle.normal.background = EditorGUIUtility.Load("Builtin Skins/DarkSkin/Images/ArrowNavigationRight.png") as Texture2D;
            _outPointStyle.active.background = EditorGUIUtility.Load("Builtin Skins/DarkSkin/Images/ArrowNavigationRight.png") as Texture2D;
        }
 
        private static List<DependencyNode> _nodes;
        private static List<Connection> _connections;
        private static void FillNodes()
        {
            _nodes = new List<DependencyNode>();
            _connections = new List<Connection>();
            _nodes.Add(CreateNode("Test1", Vector2.zero));
            _nodes.Add(CreateNode("Test2", Vector2.zero + new Vector2(400, 0)));
            _connections.Add(new Connection(_nodes[0].outPoint, _nodes[1].inPoint));
        }

        private static DependencyNode CreateNode(string title, Vector2 position)
        {
            return new DependencyNode(title, position, 300, 80, _nodeStyle, _inPointStyle, _outPointStyle);
        }

        private void OnGUI()
        {
            DrawGrid(20, 0.2f, Color.gray);
            DrawGrid(100, 0.4f, Color.gray);
            
            DrawNodes();
            DrawConnections();
 
            ProcessEvents(Event.current);
 
            if (GUI.changed) Repaint();
        }
 
        
        private void DrawNodes()
        {
            for (int i = 0; i < _nodes.Count; i++)
            {
                _nodes[i].Draw();
            }
        }
 
        private void DrawConnections()
        {
            for (int i = 0; i < _connections.Count; i++)
            {
                _connections[i].Draw();
            } 
        }
        
        private Vector2 _drag;
        private void ProcessEvents(Event e)
        {
            _drag = Vector2.zero;
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;
                case EventType.MouseDrag:
                    if (e.button == 0)
                    {
                        OnDrag(e.delta);
                    }
                    break;
            }
        }
        
        private void OnDrag(Vector2 delta)
        {
            _drag = delta;
 
            for (int i = 0; i < _nodes.Count; i++)
            {
                _nodes[i].Drag(delta);
            }
 
            GUI.changed = true;
        }
        
        private void ProcessContextMenu(Vector2 mousePosition)
        {
        }
        
        private Vector2 _offset;
        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);
 
            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);
 
            _offset += _drag * 0.5f;
            Vector3 newOffset = new Vector3(_offset.x % gridSpacing, _offset.y % gridSpacing, 0);
 
            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }
 
            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }
 
            Handles.color = Color.white;
            Handles.EndGUI();
        }
    }
}