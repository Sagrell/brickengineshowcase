﻿using System;
using System.Collections.Generic;
using StartupModule;
using UnityEditor;

namespace BrickEngine.Editor.Scripts
{
    public static class StartupSettingsEditorHolder
    {
        private static StartupSettings _settings;
        public static StartupSettings Settings
        {
            get
            {
                if (_settings == null)
                {
                    List<StartupSettings> allSettings = FindAssetsByType<StartupSettings>();
                    if (allSettings.Count == 0)
                    {
                        throw new Exception("Can't find startup settings! Please create one.");
                    }
                    if (allSettings.Count > 1)
                    {
                        throw new Exception("There are more than one startup settings!");
                    }

                    _settings = allSettings[0];
                }
                return _settings;
            }
        }

        public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
            List<T> assets = new List<T>();
            string[] guids = AssetDatabase.FindAssets($"t:{typeof(T)}");
            for( int i = 0; i < guids.Length; i++ )
            {
                string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
                T asset = AssetDatabase.LoadAssetAtPath<T>( assetPath );
                if( asset != null )
                {
                    assets.Add(asset);
                }
            }
            return assets;
        }
    }
}