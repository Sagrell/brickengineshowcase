﻿using System;
using UnityEngine;

namespace BrickEngine.Editor
{
    public enum ConnectionPointType { In, Out }
 
    public class ConnectionPoint
    {
        public Rect rect;
 
        public ConnectionPointType type;
 
        public DependencyNode node;
 
        public GUIStyle style;
 
    
        public ConnectionPoint(DependencyNode node, ConnectionPointType type, GUIStyle style)
        {
            this.node = node;
            this.type = type;
            this.style = style;
            rect = new Rect(0, 0, 10f, 20f);
        }
 
        public void Draw()
        {
            rect.y = node.Rect.y + (node.Rect.height * 0.5f) - rect.height * 0.5f;
 
            switch (type)
            {
                case ConnectionPointType.In:
                    rect.x = node.Rect.x - rect.width + 8f;
                    break;
 
                case ConnectionPointType.Out:
                    rect.x = node.Rect.x + node.Rect.width - 8f;
                    break;
            }
        }
    }
}