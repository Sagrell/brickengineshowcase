﻿using System;
using System.Collections.Generic;

namespace BrickEngine
{
	public class DisposableCollector : IDisposable
	{
		private readonly List<IDisposable> _disposables = new List<IDisposable>();

		protected void AddDisposable(IDisposable disposable)
		{
			_disposables.Add(disposable);
		}

		public virtual void Dispose()
		{
			for (int i = 0; i < _disposables.Count; i++)
			{
				_disposables[i].Dispose();
			}
		}
	}
}