using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine;

namespace BrickEngine
{
    public class EnvironmentHolder
    {
        public readonly BrickEnvironment Environment;
        public readonly List<EEnvironmentType> RequiredEnvironments;

        public EnvironmentHolder(BrickEnvironment environment, List<EEnvironmentType>requiredEnvironments)
        {
            Environment = environment;
            RequiredEnvironments = requiredEnvironments;
        }
    }

    public static class EnvironmentsManager
    {
        private static DependencyContainer _dependencyContainer;
        public static DependencyContainer GlobalContainerDebug => _dependencyContainer;

        private static readonly Dictionary<EEnvironmentType, BrickEnvironment> _allEnvironments =
            new Dictionary<EEnvironmentType, BrickEnvironment>();
        
        private static readonly Dictionary<EEnvironmentType, EnvironmentHolder> _environments =
            new Dictionary<EEnvironmentType, EnvironmentHolder>();

        private static readonly List<EnvironmentHolder> _loadedEnvironments = new List<EnvironmentHolder>();

        private static bool _checkLeaks;
        private static MemoryLeaksObserver _leaksObserver;

        public static void Init(bool checkLeaks)
        {
            Clear();
            List<Type> allEnvironments = AssemblyScanner.GetAllTypes<BrickEnvironment>();
            for (int i = 0; i < allEnvironments.Count; i++)
            {
                Type environmentType = allEnvironments[i];
                BrickEnvironment environment = (BrickEnvironment)Activator.CreateInstance(environmentType);
                _allEnvironments[environment.EnvironmentType] = environment;
            }
            _checkLeaks = checkLeaks;
            _loadedEnvironments.Clear();
            _leaksObserver = new MemoryLeaksObserver();
            _dependencyContainer = new DependencyContainer(null, null, _checkLeaks);
            _dependencyContainer.Register<IDependencyResolver>(_dependencyContainer, false);
            if (_checkLeaks)
            {
                _dependencyContainer.OnTempCreated += _leaksObserver.AddMemoryReference;
            }
        }

        public static bool HasLoadedEnvironments => _loadedEnvironments.Count > 0;
        private static void Clear()
        {
            _environments.Clear();
            _allEnvironments.Clear();
        }

        public static void RegisterConfig(StartupConfig config)
        {
            for (int i = 0; i < config.Environments.Count; i++)
            {
                StartupEnvironmentConfig environmentConfig = config.Environments[i];
                BrickEnvironment environment = _allEnvironments[environmentConfig.Type];
                EnvironmentHolder holder = new EnvironmentHolder(environment, environmentConfig.Required);
                _environments[environment.EnvironmentType] = holder;
            }
        }

        public static async UniTask<BrickEnvironment> LoadEnvironment(EEnvironmentType environmentId, ProgressNode progress = null)
        {
            if (!_environments.ContainsKey(environmentId))
            {
                throw new Exception($"Can't find environment with id = {environmentId}");
            }


            EnvironmentHolder holder = _environments[environmentId];

            if (holder.Environment.IsLoaded)
            {
                throw new Exception($"Environment with id = {environmentId} is already loaded!");
            }


            ProgressNode unloadProgress = null;
            if (_loadedEnvironments.Count > 0)
            {
                unloadProgress = progress?.CreateChild();
            }
            
            ProgressNode loadRequiredProgress = null;
            if (holder.RequiredEnvironments != null && holder.RequiredEnvironments.Count > 0)
            {
                loadRequiredProgress = progress?.CreateChild();
            }
            
            ProgressNode initializeProgress = progress?.CreateChild();
            await UnloadEnvironmentsExceptRequired(holder, unloadProgress);

            if (holder.RequiredEnvironments != null && holder.RequiredEnvironments.Count > 0)
            {
                List<ProgressNode> progresses = loadRequiredProgress?.CreateChildren(holder.RequiredEnvironments.Count);
                
                for (int i = 0; i < holder.RequiredEnvironments.Count; i++)
                {
                    EnvironmentHolder required = _environments[holder.RequiredEnvironments[i]];
                    ProgressNode loadProgress = progresses?[i];
                    if (!required.Environment.IsLoaded)
                    {
                        await LoadEnvironment(required.Environment.EnvironmentType, loadProgress);
                    }
                    else
                    {
                        loadProgress?.CompleteStep();
                    }
                }

                if (progresses != null)
                {
                    ListPool<ProgressNode>.Release(progresses);
                }
            }

            if (EnvironmentsConstants.EnableLoadingLogs)
            {
                Debug.Log("LoadEnvironment. Initialize");
            }

            await holder.Environment.Load(_dependencyContainer, _leaksObserver, initializeProgress, _checkLeaks);

            _loadedEnvironments.Add(holder);
            return holder.Environment;
        }

        public static async UniTask UnloadAllEnvironments(ProgressNode progress = null)
        {
            if (_loadedEnvironments.Count == 0)
            {
                progress?.CompleteAll();
                return;
            }
            
            List<ProgressNode> progresses = progress?.CreateChildren(_loadedEnvironments.Count);
            for (int i = _loadedEnvironments.Count - 1; i >= 0; i--)
            {
                EnvironmentHolder environmentHolder = _loadedEnvironments[i];
                ProgressNode unloadProgress = progresses?[i];
                await UnloadEnvironmentInner(environmentHolder.Environment, unloadProgress);
                unloadProgress?.CompleteStep();
            }
            
            if (progresses != null)
            {
                ListPool<ProgressNode>.Release(progresses);
            }
            
            _loadedEnvironments.Clear();

            await Resources.UnloadUnusedAssets();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private static async UniTask UnloadEnvironmentsExceptRequired(EnvironmentHolder holder, ProgressNode progress = null)
        {
            if (_loadedEnvironments.Count == 0)
            {
                progress?.CompleteAll();
                return;
            }
            
            List<ProgressNode> progresses = progress?.CreateChildren(_loadedEnvironments.Count);
            List<EEnvironmentType> allRequired = ListPool<EEnvironmentType>.Get();
            FillRequired(holder, allRequired);
            for (int i = _loadedEnvironments.Count - 1; i >= 0; i--)
            {
                EnvironmentHolder curLoaded = _loadedEnvironments[i];
                if (allRequired.Contains(curLoaded.Environment.EnvironmentType))
                {
                    progresses?[i].CompleteStep();
                    continue;
                }
                await UnloadEnvironmentInner(curLoaded.Environment, progresses?[i]);
                _loadedEnvironments.RemoveAt(i);
            }
            ListPool<EEnvironmentType>.Release(allRequired);
            if (progresses != null)
            {
                ListPool<ProgressNode>.Release(progresses);
            }
            
            await Resources.UnloadUnusedAssets();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private static void FillRequired(EnvironmentHolder holder, List<EEnvironmentType> allRequired)
        {
            if(holder.RequiredEnvironments.Count <= 0) return;
            
            List<EEnvironmentType> required = holder.RequiredEnvironments;
            allRequired.AddRange(required);
            for (int i = 0; i < required.Count; i++)
            {
                EEnvironmentType current = required[i];
                EnvironmentHolder currentEnvironment = _environments[current];
                FillRequired(currentEnvironment, allRequired);
            }
        }
        
        public static async UniTask UnloadEnvironment(EEnvironmentType environmentId, ProgressNode progress = null)
        {
            if (_environments.Count == 0 || !_environments.ContainsKey(environmentId))
            {
                throw new Exception($"Can't find environment with id = {environmentId}");
            }

            BrickEnvironment curEnvironment = _environments[environmentId].Environment;
            if (!curEnvironment.IsLoaded)
            {
                progress?.CompleteAll();
                return;
            }

            await UnloadEnvironmentInner(curEnvironment, progress);

            Resources.UnloadUnusedAssets();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private static async UniTask UnloadEnvironmentInner(BrickEnvironment environment, ProgressNode progress = null)
        {
            await environment.Dispose(progress);
        }
    }
}