﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace BrickEngine
{
    public interface IObjectLoader
    {
        T InstantiatePrefab<T>(string prefabName, Transform parent = null) where T : MonoBehaviour;
        GameObject InstantiatePrefab(string prefabName, Transform parent = null);
        UniTask<T> InstantiateAsync<T>(string prefabName, Transform parent = null) where T : MonoBehaviour;
        UniTask<GameObject> LoadPrefab(string objectName);
        UniTask<AudioClip> LoadAudio(string audioName);
        UniTask<T> LoadObject<T>(string objectName) where T : Object;
        UniTask LoadObject(string objectName);
        T GetObject<T>(string objectName) where T : Object;
        UniTask Unload(string prefabName);
    }
}