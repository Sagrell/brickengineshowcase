﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BrickEngine.Validator;

namespace BrickEngine
{
	public enum EDependencyScope
	{
		Global = 0,
		Environment = 1,
		Module = 2,
	}

	internal class Dependency
	{
		public IDependencyContext Context;
		public Type ObjType;
		public bool IgnoreValidation;
	}

	internal class SingletonClass
	{
		public object Object;
		public int RefererCount;

		public SingletonClass(object o, int refererCount = 0)
		{
			Object = o;
			RefererCount = refererCount;
		}
	}

	public sealed class DependencyContainer : IDependencyResolver
	{
		private static readonly Type CollectionType = typeof(IDependencyCollection);
		
		public event Action<object, IDependencyContext> OnTempCreated;

		private readonly Dictionary<Type, Dependency> _dependencies = new Dictionary<Type, Dependency>();

		private readonly Dictionary<Type, SingletonClass> _singletons = new Dictionary<Type, SingletonClass>();
		

		public readonly DependencyContainer Parent;
		private readonly IDependencyContext _context;
		private readonly bool _monitorTempDependencies;

		public DependencyContainer(IDependencyContext context, DependencyContainer parent, bool monitorTempDependencies= false)
		{
			_context = context;
			Parent = parent;
			_monitorTempDependencies = monitorTempDependencies;
		}

		public void Register<TInterface>(TInterface singleton, bool ignoreValidation)
		{
			Type type = typeof(TInterface);
			if (Contains(type))
			{
				throw new Exception($"Dependency of type = {type} already exists!");
			}

			Type impType = singleton.GetType();
			if (!_singletons.ContainsKey(impType))
			{
				_singletons[impType] = new SingletonClass(singleton);
			}

			Dependency dependency = new Dependency
			{
				IgnoreValidation = ignoreValidation,
				Context = _context,
				ObjType = impType
			};
			_dependencies[type] = dependency;
		}

		public void Register<T>(IDependencyContext context)
		{
			Type type = typeof(T);
			if (Contains(type, context))
			{
				throw new Exception($"Dependency of type = {type} already exists!");
			}

			Dependency dependency = new Dependency
			{
				Context = context,
				ObjType = type
			};
			_dependencies[type] = dependency;
		}

		public void Register<TInterface, TImplementation>(IDependencyContext context)
			where TImplementation : TInterface
		{
			Type type = typeof(TInterface);
			if (Contains(type, context))
			{
				throw new Exception($"Dependency of type = {type} already exists!");
			}

			Type impType = typeof(TImplementation);
			Dependency dependency = new Dependency
			{
				Context = context,
				ObjType = impType
			};
			_dependencies[type] = dependency;
		}
		
		private bool Contains(Type type, IDependencyContext context)
		{
			if (TryGetDependency(type, out Dependency dependency))
			{
				return dependency.Context == context;
			}

			return false;
		}

		public T Resolve<T>(params object[] args)
		{
			Type type = typeof(T);
			return (T) Resolve(type, args);
		}

		public bool Contains(Type type)
		{
			return TryGetContainerWithDependency(type, out _, out _);
		}

		public object Resolve(Type type, params object[] args)
		{
			if (CollectionType.IsAssignableFrom(type))
			{
				IDependencyCollection collection = (IDependencyCollection)Activator.CreateInstance(type);
				return ResolveCollection(collection);
			}
			
			if (!TryGetContainerWithDependency(type, out DependencyContainer container, out Dependency dependency))
			{
				object obj = Create(type, args);
				if (_monitorTempDependencies)
				{
					OnTempCreated?.Invoke(obj, null);
				}
				return obj;
			}

			return container.Resolve(dependency, args);
		}

		private object Resolve(Dependency dependency, params object[] args)
		{
			object resolved;
			if (!_singletons.ContainsKey(dependency.ObjType))
			{
				resolved = Create(dependency.ObjType, args);
				_singletons.Add(dependency.ObjType, new SingletonClass(resolved));
			}
			else
			{
				SingletonClass singleton = _singletons[dependency.ObjType];
				resolved = singleton.Object;
				singleton.RefererCount++;
			}

			return resolved;
		}

		private object Create(Type type, params object[] args)
		{
			if (type.IsAbstract)
			{
				throw new Exception($"Can't create an abstract object of type {type.FullName}");
			}

			ConstructorInfo[] constructors = type.GetConstructors();
			if (constructors.Length == 0)
			{
				return Activator.CreateInstance(type);
			}

			if (constructors.Length > 1)
			{
				throw new Exception($"There are more than 1 constructors on object of type {type.FullName}");
			}

			ConstructorInfo constructorInfo = constructors[0];
			ParameterInfo[] parameters = constructorInfo.GetParameters();

			bool hasArgs = args.Length > 0;
			object[] totalArgs = new object[parameters.Length];
			List<object> listArgs = ListPool<object>.Get();
			listArgs.Fill(args);
			for (int parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
			{
				Type parameterType = parameters[parameterIndex].ParameterType;
				bool isFilled = false;
				if (hasArgs)
				{
					for (int i = listArgs.Count - 1; i >= 0; i--)
					{
						object arg = listArgs[i];
						if (IsSuitable(parameterType, arg.GetType()))
						{
							totalArgs[parameterIndex] = arg;
							isFilled = true;
							listArgs.RemoveAt(i);
							break;
						}
					}
				}

				if (!isFilled)
				{
					totalArgs[parameterIndex] = Resolve(parameterType, type);
				}
			}

			return constructorInfo.Invoke(totalArgs);
		}

		private bool IsSuitable(Type srcType, Type type)
		{
			bool isSameType = type == srcType;
			bool isSubclass = srcType.IsAssignableFrom(type);
			bool isSuitable = isSameType || isSubclass;
			return isSuitable;
		}
		
		public void ResolveAll()
		{
			foreach (Dependency dependency in _dependencies.Values)
			{
				if (_singletons.ContainsKey(dependency.ObjType)) continue;

				object resolved = Create(dependency.ObjType);
				_singletons.Add(dependency.ObjType, new SingletonClass(resolved));
			}

			DependencyContainer parent = Parent;
			while (parent != null)
			{
				parent.ResolveAll(_context);
				parent = parent.Parent;
			}
		}

		private object ResolveCollection(IDependencyCollection collection)
		{
			foreach (KeyValuePair<Type, Dependency> dependencyKeyValue in _dependencies)
			{
				Dependency dependency = dependencyKeyValue.Value;
				if (collection.IsSuitable(dependency.ObjType))
				{
					object resolved = Resolve(dependencyKeyValue.Key);
					collection.Add(resolved);
				}
			}
			DependencyContainer parent = Parent;
			while (parent != null)
			{
				parent.ResolveCollection(collection);
				parent = parent.Parent;
			}
			return collection;
		}
		
		public List<object> GetSingletons()
		{
			List<object> singletons = _singletons.Values.Select(s => s.Object).ToList();
			DependencyContainer parent = Parent;
			while (parent != null)
			{
				singletons.AddRange(parent.GetSingletons(_context));
				parent = parent.Parent;
			}

			return singletons;
		}

		public void Dispose()
		{
			foreach (SingletonClass singleton in _singletons.Values)
			{
				if (singleton.Object is IDisposable disposable)
				{
					disposable.Dispose();
				}
			}
			
			_singletons.Clear();
			_dependencies.Clear();
			OnTempCreated = null;

			DependencyContainer parent = Parent;
			while (parent != null)
			{
				parent.Dispose(_context);
				parent = parent.Parent;
			}
		}

		private void Dispose(IDependencyContext context)
		{
			List<SingletonClass> singletonsToDispose = ListPool<SingletonClass>.Get();
			List<Type> dependencyToRemove = ListPool<Type>.Get();
			foreach (KeyValuePair<Type, Dependency> dependencyKeyValue in _dependencies)
			{
				Dependency dependency = dependencyKeyValue.Value;
				if (dependency.Context != context) continue;
				if (_singletons.ContainsKey(dependency.ObjType))
				{
					SingletonClass singleton = _singletons[dependency.ObjType];
					singletonsToDispose.Add(singleton);
					_singletons.Remove(dependency.ObjType);
				}

				dependencyToRemove.Add(dependencyKeyValue.Key);
			}

			for (int i = 0; i < singletonsToDispose.Count; i++)
			{
				if (singletonsToDispose[i].Object is IDisposable disposable)
				{
					disposable.Dispose();
				}
			}

			for (int i = 0; i < dependencyToRemove.Count; i++)
			{
				_dependencies.Remove(dependencyToRemove[i]);
			}

			ListPool<SingletonClass>.Release(singletonsToDispose);
			ListPool<Type>.Release(dependencyToRemove);
		}

		private List<object> GetSingletons(IDependencyContext context)
		{
			List<object> singletons = new List<object>();
			foreach (Dependency dependency in _dependencies.Values)
			{
				if (dependency.Context != context ||
					!_singletons.ContainsKey(dependency.ObjType)) continue;

				singletons.Add(_singletons[dependency.ObjType].Object);
			}

			return singletons;
		}

		private void ResolveAll(IDependencyContext context)
		{
			foreach (Dependency dependency in _dependencies.Values)
			{
				if (dependency.Context != context ||
					_singletons.ContainsKey(dependency.ObjType)) continue;

				object resolved = Create(dependency.ObjType);
				_singletons.Add(dependency.ObjType, new SingletonClass(resolved));
			}
		}

		private bool TryGetContainerWithDependency(Type type, out DependencyContainer container,
												   out Dependency dependency)
		{
			if (_dependencies.TryGetValue(type, out dependency))
			{
				container = this;
				return true;
			}

			DependencyContainer parent = Parent;
			while (parent != null)
			{
				if (parent.TryGetDependency(type, out dependency))
				{
					container = parent;
					return true;
				}

				parent = parent.Parent;
			}

			container = null;
			return false;
		}

		private bool TryGetDependency(Type type, out Dependency dependency)
		{
			if (_dependencies.TryGetValue(type, out dependency))
			{
				return true;
			}

			DependencyContainer parent = Parent;
			while (parent != null)
			{
				if (parent.TryGetDependency(type, out dependency))
				{
					return true;
				}

				parent = parent.Parent;
			}

			return false;
		}
		
		private bool ContainsAssignable(Type type)
		{
			foreach (Type dependencyType in _dependencies.Keys)
			{
				if (type == dependencyType || dependencyType.IsAssignableFrom(type))
				{
					return true;
				}
			}

			DependencyContainer parent = Parent;
			while (parent != null)
			{
				if (parent.ContainsAssignable(type))
				{
					return true;
				}

				parent = parent.Parent;
			}

			return false;
		}

	#region Validation

		public void Validate(IValidateErrorHandler errorHandler, ValidateGraph graph, bool validateCollections)
		{
			foreach (KeyValuePair<Type, Dependency> dependencyKeyValue in _dependencies)
			{
				Validate(dependencyKeyValue.Key, null, graph, errorHandler, validateCollections);
			}
			DependencyContainer parent = Parent;
			while (parent != null)
			{
				parent.Validate(errorHandler, graph, validateCollections);
				parent = parent.Parent;
			}
		}

		private void ValidateCollectionDependency(IDependencyCollection collection, Type type, ValidateGraph graph, IValidateErrorHandler errorHandler, bool validateCollections)
		{
			if (validateCollections)
			{
				List<Type> allTypes = AssemblyScanner.GetAllTypes(collection.Type);
				for (int i = 0; i < allTypes.Count; i++)
				{
					if (!ContainsAssignable(allTypes[i]))
					{
						errorHandler.HandleError($"Type {allTypes[i].Name} is not registered for collection of type {collection.Type.Name}");
						return;
					}
				}
			}
			
			ValidateCollection(collection, type, graph, errorHandler, validateCollections);
		}
		
		private void ValidateCollection(IDependencyCollection collection, Type type, ValidateGraph graph, IValidateErrorHandler errorHandler, bool validateCollections)
		{
			foreach (KeyValuePair<Type, Dependency> dependencyKeyValue in _dependencies)
			{
				Dependency dependency = dependencyKeyValue.Value;
				if (collection.IsSuitable(dependency.ObjType))
				{
					Validate(dependencyKeyValue.Key, type, graph, errorHandler, validateCollections);
				}
			}
			DependencyContainer parent = Parent;
			while (parent != null)
			{
				parent.ValidateCollection(collection, type, graph, errorHandler, validateCollections);
				parent = parent.Parent;
			}
		}
		
		private void Validate(Type type, Type root, ValidateGraph graph, IValidateErrorHandler errorHandler, bool validateCollections)
		{
			if (!TryGetDependency(type, out Dependency dependency))
			{
				errorHandler.HandleError($"Can't find dependency of type {type} in class {root}!");
				return;
			}

			if (dependency.IgnoreValidation)
			{
				graph.Visit(type);
				return;
			}
			
			Type objType = dependency.ObjType;

			if (graph.IsVisited(objType)) return;

			ConstructorInfo[] constructors = objType.GetConstructors();
			if (constructors.Length == 0)
			{
				graph.Visit(type);
				return;
			}

			if (constructors.Length > 1)
			{
				errorHandler.HandleError($"There are more than 1 constructors on object of type {objType.FullName}");
				return;
			}

			graph.Visit(objType);
			graph.AddCycle(objType);
			ConstructorInfo constructorInfo = constructors[0];
			ParameterInfo[] parameters = constructorInfo.GetParameters();

			for (int parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
			{
				Type parameterType = parameters[parameterIndex].ParameterType;
				if (CollectionType.IsAssignableFrom(parameterType))
				{
					IDependencyCollection collection = (IDependencyCollection)Activator.CreateInstance(parameterType);
					ValidateCollectionDependency(collection, objType, graph, errorHandler, validateCollections);
					continue;
				}
				
				if (!TryGetDependency(parameterType, out Dependency paramDependency))
				{
					errorHandler.HandleError($"Can't find dependency of type {parameterType} in class {objType}!");
					continue;
				}

				Type paramObjType = paramDependency.ObjType;
				if (graph.IsVisited(paramObjType))
				{
					if (graph.IsCycle(paramObjType))
					{
						throw new Exception($"There is an cycle between {objType} and {paramObjType}!");
					}

					continue;
				}

				Validate(parameterType, objType, graph, errorHandler, validateCollections);

				if (graph.IsCycle(paramObjType))
				{
					throw new Exception($"There is an cycle between {objType} and {parameterType}!");
				}
			}

			graph.RemoveCycle(objType);
		}

	#endregion
	}
}