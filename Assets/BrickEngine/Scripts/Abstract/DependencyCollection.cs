﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BrickEngine
{
    public sealed class DependencyCollection<T> : IDependencyCollection
    {
        public Type Type { get; }

        public IReadOnlyList<T> Collection => _collection.ToList();

        private readonly HashSet<T> _collection;

        public DependencyCollection()
        {
            _collection = new HashSet<T>();
            Type = typeof(T);
        }

        public bool IsSuitable(Type type)
        {
            var isSameType = type == Type;
            var isSubclass = Type.IsAssignableFrom(type);
            var isSuitable = isSameType || isSubclass;
            return isSuitable;
        }

        public void Add(object obj)
        {
            _collection.Add((T) obj);
        }


        public void AddRange(List<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                _collection.Add(list[i]);
            }
        }
    }

    public interface IDependencyCollection
    {
        bool IsSuitable(Type type);
        void Add(object obj);
        Type Type { get; }
    }
}