using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace BrickEngine
{
	public abstract class LoadableModuleInitializer : ModuleInitializer
	{
		private IObjectLoader _loader;
		public List<UniTask> GetTasks(IObjectLoader loader)
		{
			_loader = loader;
			List<UniTask> tasks = ListPool<UniTask>.Get();
			for (int i = 0; i < _loadedPrefabs.Count; i++)
			{
				tasks.Add(loader.LoadPrefab(_loadedPrefabs[i]));
			}
			
			for (int i = 0; i < _loadedObjects.Count; i++)
			{
				tasks.Add(loader.LoadObject(_loadedObjects[i]));
			}
			return tasks;
		}
		
		private readonly List<string>  _loadedObjects = new List<string>();
		private readonly List<string>  _loadedPrefabs = new List<string>();

		protected void LoadPrefab(string objectName)
		{
			_loadedPrefabs.Add(objectName);
		}

		protected void LoadObject(string objectName)
		{
			_loadedObjects.Add(objectName);
		}

		public void Unload()
		{
			for (int i = 0; i < _loadedObjects.Count; i++)
			{
				_loader.Unload(_loadedObjects[i]);
			}
			
			for (int i = 0; i < _loadedPrefabs.Count; i++)
			{
				_loader.Unload(_loadedPrefabs[i]);
			}

			_loader = null;
		}
	}
}