﻿using System;
using BrickEngine.Validator;

namespace BrickEngine
{
	public abstract class ModuleInitializer : IDisposable, IDependencyContext
	{
		public DependencyContainer Container => _moduleContainer;
		
		private int                 _contextId;
		private DependencyContainer _globalContainer;
		private DependencyContainer _envContainer;
		private DependencyContainer _moduleContainer;

		public void InitContainers(DependencyContainer globalContainer, DependencyContainer envContainer)
		{
			_globalContainer = globalContainer;
			_envContainer = envContainer;
			_moduleContainer = new DependencyContainer(this, _envContainer);
		}

		public abstract void Initialize();

		protected void Register<T>(EDependencyScope scope = EDependencyScope.Global) where T : class
		{
			switch (scope)
			{
				case EDependencyScope.Global:
					_globalContainer.Register<T>(this);
					break;
				case EDependencyScope.Environment:
					_envContainer.Register<T>(this);
					break;
				case EDependencyScope.Module:
					_moduleContainer.Register<T>(this);
					break;
			}
		}

		protected void Register<TInterface, TImplementation>(EDependencyScope scope = EDependencyScope.Global) where TImplementation : class, TInterface
		{
			switch (scope)
			{
				case EDependencyScope.Global:
					_globalContainer.Register<TInterface, TImplementation>(this);
					break;
				case EDependencyScope.Environment:
					_envContainer.Register<TInterface, TImplementation>(this);
					break;
				case EDependencyScope.Module:
					_moduleContainer.Register<TInterface, TImplementation>(this);
					break;
			}
		}
		
		public void ResolveAll()
		{
			_moduleContainer.ResolveAll();
		}

		public void ValidateDependencies(IValidateErrorHandler errorHandler, ValidateGraph graph, bool validateCollections)
		{
			_moduleContainer.Validate(errorHandler, graph, validateCollections);
		}

		public void Dispose()
		{
			_moduleContainer.Dispose();
		}
	}
}