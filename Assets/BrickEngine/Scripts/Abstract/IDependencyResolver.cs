﻿using System;

namespace BrickEngine
{
	public interface IDependencyResolver
	{
		T Resolve<T>(params object[] args);
		bool Contains(Type type);
	}
}