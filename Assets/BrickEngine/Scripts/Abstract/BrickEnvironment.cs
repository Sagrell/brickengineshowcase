﻿using System.Collections.Generic;
using System.Diagnostics;
using BrickEngine.Unity;
using BrickEngine.Validator;
using Cysharp.Threading.Tasks;
using GameModule;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace BrickEngine
{
	public abstract class BrickEnvironment : IDependencyContext
	{
		public abstract EEnvironmentType EnvironmentType { get; }

		public bool IsLoaded { get; private set; }

		private readonly List<ModuleInitializer> _gameModules = new List<ModuleInitializer>();

		protected void Add<T>() where T : ModuleInitializer, new()
		{
			T module = new T();
			_gameModules.Add(module);
		}

		public void Validate(DependencyContainer global, ValidateGraph graph, IValidateErrorHandler errorHandler, bool validateCollections)
		{
			_envContainer = new DependencyContainer(this, global);
			FillModules();
			int gameModulesCount = _gameModules.Count;
			for (int i = 0; i < gameModulesCount; i++)
			{
				ModuleInitializer module = _gameModules[i];
				module.InitContainers(global, _envContainer);
				module.Initialize();
			}

			for (int i = 0; i < gameModulesCount; i++)
			{
				_gameModules[i].ValidateDependencies(errorHandler, graph, validateCollections);
			}
		}

		private DependencyContainer _envContainer;
		private MemoryLeaksObserver _leaksObserver;
		private bool _checkLeaks;

		internal async UniTask Load(DependencyContainer globalContainer, MemoryLeaksObserver leaksObserver, ProgressNode progress = null, bool checkLeaks = false)
		{
			_leaksObserver = leaksObserver;
			_checkLeaks = checkLeaks;
			_envContainer = new DependencyContainer(this, globalContainer);
			if (checkLeaks)
			{
				_envContainer.OnTempCreated += _leaksObserver.AddMemoryReference;
			}
			FillModules();
			int gameModulesCount = _gameModules.Count;
			if (gameModulesCount == 0)
			{
				progress?.CompleteAll();
				return;
			}

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();


			if (EnvironmentsConstants.EnableLoadingLogs)
				Debug.Log("LoadEnvironment. Loading");
			
			ProgressNode initializeProgress = progress?.CreateChild("Initialize environment " + EnvironmentType);
			ProgressNode loadModulesProgress = progress?.CreateChild("Load modules in environment " + EnvironmentType);
			ProgressNode loadEnvironmentProgress = progress?.CreateChild();
			ProgressNode resolveProgress = progress?.CreateChild("Resolve environment " + EnvironmentType);

			initializeProgress?.SetTitle();
			for (int i = 0; i < gameModulesCount; i++)
			{
				ModuleInitializer module = _gameModules[i];
				if (EnvironmentsConstants.EnableLoadingLogs)
					Debug.Log($"LoadEnvironment. Loading {_gameModules[i].GetType()} Started");

				module.InitContainers(globalContainer, _envContainer);
				if (checkLeaks)
				{
					module.Container.OnTempCreated += _leaksObserver.AddMemoryReference;
				}
				module.Initialize();

				if (EnvironmentsConstants.EnableLoadingLogs)
					Debug.Log($"LoadEnvironment. Loading {_gameModules[i].GetType()} Complete");
				
			}
			initializeProgress?.CompleteAll();

			Debug.Log($"Environment {EnvironmentType} initialize time: {(float) stopwatch.ElapsedMilliseconds / 1000}s");

			loadModulesProgress?.SetTitle();
			List<UniTask> tasks = ListPool<UniTask>.Get();
			for (int i = 0; i < gameModulesCount; i++)
			{
				ModuleInitializer module = _gameModules[i];
				if (module is LoadableModuleInitializer loader)
				{
					IObjectLoader objectLoader = Resolve<IObjectLoader>();
					List<UniTask> moduleTasks = loader.GetTasks(objectLoader);
					tasks.AddRange(moduleTasks);
					ListPool<UniTask>.Release(moduleTasks);
				}
			}

			stopwatch.Reset();
			stopwatch.Start();
			await UniTask.WhenAll(tasks);
			ListPool<UniTask>.Release(tasks);
			loadModulesProgress?.CompleteAll();
			await Load(loadEnvironmentProgress);
			Debug.Log($"Environment {EnvironmentType} load time: {(float) stopwatch.ElapsedMilliseconds / 1000}s");

			resolveProgress?.SetTitle();
			ResolveAll();
			if (checkLeaks)
			{
				FillMemoryReferences();
			}
			resolveProgress?.CompleteAll();
			OnEnvironmentReady();
			IsLoaded = true;
		}


		public T Resolve<T>()
		{
			return _envContainer.Resolve<T>();
		}

		protected virtual void OnEnvironmentReady() { }

		protected virtual UniTask Load(ProgressNode progressNode = null)
		{
			return UniTask.CompletedTask;
		}


		private readonly List<int> _loadedScenes = new List<int>();

		protected async UniTask LoadScene(int index, LoadSceneMode sceneMode)
		{
			_loadedScenes.Add(index);
			await ScenesController.LoadSceneAsync(index, sceneMode);
			ScenesController.SetActiveScene(index);
		}

		protected abstract void FillModules();
		
		internal async UniTask Dispose(ProgressNode progress = null)
		{
			int gameModulesCount = _gameModules.Count;
			if (gameModulesCount == 0)
			{
				progress?.CompleteAll();
				return;
			}

			ProgressNode unloadProgress = progress?.CreateChild("Unload modules", gameModulesCount);
			ProgressNode unloadScenesProgress = progress?.CreateChild("Unload scenes", _loadedScenes.Count);
			ProgressNode checkLeaksProgress = progress?.CreateChild("Check leaks");
			unloadProgress?.SetTitle();
			for (int i = gameModulesCount - 1; i >= 0; i--)
			{
				ModuleInitializer module = _gameModules[i];
				if (module is LoadableModuleInitializer loader)
				{
					loader.Unload();
				}

				module.Dispose();
				unloadProgress?.CompleteStep();
			}
			
			_envContainer.Dispose();
			unloadScenesProgress?.SetTitle();
			for (int i = 0; i < _loadedScenes.Count; i++)
			{
				await ScenesController.UnloadSceneAsync(_loadedScenes[i]);
				unloadScenesProgress?.CompleteStep();
			}
			
			if (_checkLeaks)
			{
				checkLeaksProgress?.SetTitle();
				await CheckLeaks();
				checkLeaksProgress?.CompleteAll();
			}
			
			_gameModules.Clear();
			_loadedScenes.Clear();
			IsLoaded = false;
		}

		private void ResolveAll()
		{
			for (int i = 0; i < _gameModules.Count; i++)
			{
				ModuleInitializer module = _gameModules[i];
				module.ResolveAll();
			}
		}
		
		private async UniTask CheckLeaks()
		{
			for (int i = 0; i < _gameModules.Count; i++)
			{
				ModuleInitializer module = _gameModules[i];

				if (_checkLeaks)
				{
					await _leaksObserver.CheckLeaksAndRemove(module);
				}
			}
		}
		
		private void FillMemoryReferences()
		{
			for (int i = 0; i < _gameModules.Count; i++)
			{
				ModuleInitializer module = _gameModules[i];
				if (_checkLeaks)
				{
					_leaksObserver.FillMemoryReferences(module.Container.GetSingletons(), module);
				}
			}
		}
	}
}