﻿namespace BrickEngine
{
    public static class DependencyFactory
    {
        private static readonly IDependencyResolver _dependencyResolver;
        static DependencyFactory()
        {
            _dependencyResolver = EnvironmentsManager.GlobalContainerDebug.Resolve<IDependencyResolver>();
        }
        
        public static TObject Create<TObject>(params object[] args)
        {
            return _dependencyResolver.Resolve<TObject>(args);
        }
    }
}