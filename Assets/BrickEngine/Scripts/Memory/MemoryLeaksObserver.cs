﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace BrickEngine
{
	public class MemoryLeaksObserver
	{
		private readonly MemoryMonitor _memoryMonitor = new MemoryMonitor();

		public async UniTask<bool> CheckLeaksAndRemove(object context)
		{
			return await _memoryMonitor.CheckLeaks(context);
		}

		public void AddMemoryReference(object obj, object context)
		{
			_memoryMonitor.Register(obj, context);
		}
		
		public void FillMemoryReferences(IReadOnlyList<object> objects, object context)
		{
			for (int objIndex = 0; objIndex < objects.Count; objIndex++)
			{
				object curObject = objects[objIndex];
				_memoryMonitor.Register(curObject, context);
			}
		}

		public void Clear()
		{
			_memoryMonitor.Clear();
		}
	}
}