﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace BrickEngine
{
	public class MemoryReference
	{
		public WeakReference Reference;
		public object Context;
	}

	public class MemoryMonitor
	{
		private readonly List<MemoryReference> _references = new List<MemoryReference>();

		public void Register(object obj, object context)
		{
			_references.Add(new MemoryReference
			{
				Reference = new WeakReference(obj),
				Context = context
			});
		}

		public async UniTask<bool> CheckLeaks(object context)
		{
			_references.Reverse();
			await Resources.UnloadUnusedAssets();
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			await UniTask.DelayFrame(1);
			int leakCount = 0;
			for (int i = _references.Count - 1; i >= 0; i--)
			{
				MemoryReference reference = _references[i];
				if (reference.Context != context) continue;
				if (reference.Reference.IsAlive)
				{
					leakCount++;
					Debug.LogError($"There's memory leak! {reference.Reference.Target.GetType().Name}. Context: {context.GetType().Name}");
				}

				_references.RemoveAt(i);
			}

			return leakCount > 0;
		}

		public void Clear()
		{
			_references.Clear();
		}
	}
}