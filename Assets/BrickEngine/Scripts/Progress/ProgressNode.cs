﻿using System;
using System.Collections.Generic;

namespace BrickEngine
{
    public class ProgressNode
    {
        private const float FullProgress = 1f;
        private float _value;
        private int _count;
        private ProgressNode _parent;
        private readonly List<ProgressNode> _children = new List<ProgressNode>();

        private readonly ProgressHandler _handler;
        private readonly string _name;

        public ProgressNode(ProgressHandler handler, string name)
        {
            _handler = handler;
            _name = name;
        }

        public List<ProgressNode> CreateChildren(int count)
        {
            List<ProgressNode> progresses = ListPool<ProgressNode>.Get();
            for (int i = 0; i < count; i++)
            {
                progresses.Add(CreateChild());
            }

            return progresses;
        }
        
        public ProgressNode CreateChild(string name = null, int count = 1)
        {
            if (_count > 1)
            {
                throw new Exception("Can't create a child for the node that has count isn't equal 1");
            }

            ProgressNode node = _handler.CreateNode(name);
            _children.Add(node);
            node._parent = this;
            node._count = count;
            return node;
        }

        public void CompleteAll()
        {
            PropagateDeltaProgress(FullProgress - _value);
        }

        public void SetTitle()
        {
            _handler.SendTitle(_name);
        }
        
        public void CompleteStep()
        {
            PropagateDeltaProgress(FullProgress / _count);
        }

        public void SetProgress(float progress)
        {
            PropagateProgress(progress);
        }

        private void PropagateDeltaProgress(float delta)
        {
            if (_parent == null)
            {
                _value += delta;
                _handler.SendProgress(new ProgressData(_name, _value));
                return;
            }

            ProgressNode current = this;
            ProgressNode parent = _parent;
            while (parent != null)
            {
                current = parent;
                float childrenRatio = FullProgress / parent._children.Count;
                delta *= childrenRatio;
                parent = parent._parent;
            }

            current._value += delta;
            _handler.SendProgress(new ProgressData(_name, current._value));
        }
        
        private void PropagateProgress(float progress)
        {
            if (_parent == null)
            {
                _value = progress;
                _handler.SendProgress(new ProgressData(_name, _value));
                return;
            }

            ProgressNode current = this;
            ProgressNode parent = _parent;
            while (parent != null)
            {
                current = parent;
                float childrenRatio = FullProgress / parent._children.Count;
                progress *= childrenRatio;
                parent = parent._parent;
            }

            current._value = progress;
            _handler.SendProgress(new ProgressData(_name, current._value));
        }
    }
}