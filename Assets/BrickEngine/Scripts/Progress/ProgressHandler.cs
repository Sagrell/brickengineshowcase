using System;

namespace BrickEngine
{
    public struct ProgressData
    {
        public string ProgressTitle;
        public float Value;

        public ProgressData(string progressTitle, float value)
        {
            ProgressTitle = progressTitle;
            Value = value;
        }
    }
	
    public class ProgressHandler
    {
        public event Action<ProgressData> OnProgressChanged;

        public ProgressNode CreateNode(string name = null)
        {
            ProgressNode node = new ProgressNode(this, name);
            return node;
        }

        private float _lastProgress;
        public void SendProgress(ProgressData progressData)
        {
            OnProgressChanged?.Invoke(progressData);
            _lastProgress = progressData.Value;
        }
		
        public void SendTitle(string title)
        {
            OnProgressChanged?.Invoke(new ProgressData(title, _lastProgress));
        }
    }
}