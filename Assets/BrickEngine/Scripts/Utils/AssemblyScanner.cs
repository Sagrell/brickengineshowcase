﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BrickEngine
{
	public static class AssemblyScanner
	{
		private static readonly Dictionary<string, Assembly> _assembliesCache = new Dictionary<string, Assembly>();

		public static void CacheAssemblies(string[] assemblies)
		{
			if(_assembliesCache.Count > 0) return;
			_assembliesCache.Clear();
			AddAssemblies(assemblies);
		}

		public static List<T> GetInstances<T>(string assemblyName)
		{
			List<T> instances = new List<T>();
			var types = GetAllTypes<T>(assemblyName);
			for (int i = 0; i < types.Count; i++)
			{
				instances.Add((T) Activator.CreateInstance(types[i]));
			}

			return instances;
		}

		public static List<Type> GetAllTypes(Type neededType, string assemblyName, bool includeAbstract = false)
		{
			List<Type> list = new List<Type>();
			var assembly = GetAssembly(assemblyName);
			Type[] types = assembly.GetTypes();
			for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
			{
				Type type = types[typeIndex];
				if (neededType.IsAssignableFrom(type) && !type.IsInterface && (includeAbstract || !type.IsAbstract))
				{
					list.Add(type);
				}
			}

			return list;
		}

		public static List<Type> GetAllTypesByAttribute<TAttribute>(string assemblyName) where TAttribute : Attribute
		{
			List<Type> list = new List<Type>();
			Type neededType = typeof(TAttribute);
			var assembly = GetAssembly(assemblyName);
			Type[] types = assembly.GetTypes();
			for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
			{
				Type type = types[typeIndex];
				if (type.GetCustomAttribute(neededType) != null)
				{
					list.Add(type);
				}
			}

			return list;
		}
		
		public static List<Type> GetAllTypes(Type neededType, bool includeAbstract = false)
		{
			List<Type> list = new List<Type>();
			var assemblies = GetAssemblies();
			for (int i = 0; i < assemblies.Count; i++)
			{
				var assembly = assemblies[i];
				Type[] types = assembly.GetTypes();
				for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
				{
					Type type = types[typeIndex];
					if (neededType.IsAssignableFrom(type) && !type.IsInterface && (includeAbstract || !type.IsAbstract))
					{
						list.Add(type);
					}
				}
			}

			return list;
		}
		
		public static List<Type> GetAllTypes<T>(bool includeAbstract = false)
		{
			Type neededType = typeof(T);
			return GetAllTypes(neededType, includeAbstract);
		}
		
		
		public static List<Type> GetAllTypes<T>(string assemblyName, bool includeAbstract = false)
		{
			List<Type> list = new List<Type>();
			Type neededType = typeof(T);
			var assembly = GetAssembly(assemblyName);
			Type[] types = assembly.GetTypes();
			for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
			{
				Type type = types[typeIndex];
				if (neededType.IsAssignableFrom(type) && !type.IsInterface && (includeAbstract || !type.IsAbstract))
				{
					list.Add(type);
				}
			}

			return list;
		}

		public static Assembly GetAssembly(string assemblyName)
		{
			if (!_assembliesCache.ContainsKey(assemblyName))
			{
				throw new Exception("Can't find assemble with name = " + assemblyName);
			}

			return _assembliesCache[assemblyName];
		}

		public static Assembly GetAssembly(List<Assembly> assemblies, string assemblyName)
		{
			for (int index = 0; index < assemblies.Count; index++)
			{
				var assembly = assemblies[index];
				bool userCode = assembly.FullName.Contains(assemblyName);
				if (userCode)
				{
					return assembly;
				}
			}

			return null;
		}

		public static void AddAssemblies(string[] assemblyNames)
		{
			var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
			for (int i = 0; i < assemblyNames.Length; i++)
			{
				var assemblyName = assemblyNames[i];
				if (_assembliesCache.ContainsKey(assemblyName))
				{
					return;
				}
				for (int index = 0; index < allAssemblies.Length; index++)
				{
					var assembly = allAssemblies[index];
					bool userCode = assembly.FullName.Contains(assemblyName);
					if (userCode)
					{
						_assembliesCache[assemblyName] = assembly;
					}
				}
			}
		}

		private static List<Assembly> GetAssemblies()
		{
			List<Assembly> assemblies = new List<Assembly>();

			var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
			for (int i = 0; i < allAssemblies.Length; i++)
			{
				var assembly = allAssemblies[i];
				if (!assembly.FullName.Contains("UnityEngine") && !assembly.FullName.Contains("Editor"))
				{
					assemblies.Add(assembly);
				}
			}

			return assemblies;
		}
	}
}