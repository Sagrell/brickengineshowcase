using System;
using System.Collections.Generic;

namespace BrickEngine
{
	public class ObjectPool<T> where T : new()
	{
		private readonly Stack<T>  m_Stack = new Stack<T>();
		private readonly Action<T> m_ActionOnGet;
		private readonly Action<T> m_ActionOnRelease;

		public bool IsEmpty => m_Stack.Count == 0;
		private readonly bool _createIfEmpty;

		public ObjectPool(Action<T> actionOnGet, Action<T> actionOnRelease, bool createIfEmpty = true)
		{
			m_ActionOnGet = actionOnGet;
			m_ActionOnRelease = actionOnRelease;
			_createIfEmpty = createIfEmpty;
		}

		public T Get()
		{
			T element;
			if (m_Stack.Count == 0)
			{
				if (_createIfEmpty)
				{
					element = new T();
				}
				else
				{
					return default(T);
				}
			}
			else
			{
				element = m_Stack.Pop();
			}

			m_ActionOnGet?.Invoke(element);
			return element;
		}

		public void Release(T element)
		{
			m_ActionOnRelease?.Invoke(element);
			m_Stack.Push(element);
		}

		public void Clear()
		{
			m_Stack.Clear();
		}
	}

	public class SimpleObjectPool<T>
	{
		private readonly ExtendedStack<T> _stack = new ExtendedStack<T>();
		public bool IsEmpty => _stack.Count == 0;
		public List<T> ListItems => _stack.ListItems;

		public bool Contains(T value)
		{
			return _stack.Contains(value);
		}

		public bool TryGet(out T result)
		{
			if (_stack.Count == 0)
			{
				result = default;
				return false;
			}

			T element = _stack.Pop();
			result = element;
			return true;
		}

		public void Release(T element)
		{
			_stack.Push(element);
		}

		public void Clear()
		{
			_stack.Clear();
		}
	}
}