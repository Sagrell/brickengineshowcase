using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace BrickEngine.Unity
{
	public static class ScenesController
	{
		public static void LoadScene(int index, LoadSceneMode sceneMode)
		{
			SceneManager.LoadScene(index, sceneMode);
		}

		public static int SceneCount => SceneManager.sceneCount;

		public static int GetActiveScene()
		{
			return SceneManager.GetActiveScene().buildIndex;
		}

		public static int GetActiveScene(int index)
		{
			return SceneManager.GetSceneAt(index).buildIndex;
		}

		public static async UniTask LoadAndActiveSceneAsync(int index)
		{
			await SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
			SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(index));
		}

		public static async UniTask LoadSceneAsync(int index, LoadSceneMode sceneMode)
		{
			await SceneManager.LoadSceneAsync(index, sceneMode);
		}

		public static void SetActiveScene(int index)
		{
			SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(index));
		}

		public static async UniTask UnloadSceneAsync(
			int index, UnloadSceneOptions unloadSceneOptions = UnloadSceneOptions.None)
		{
			await SceneManager.UnloadSceneAsync(index, unloadSceneOptions);
		}
	}
}