using System;
using System.Collections.Generic;

namespace BrickEngine
{
	public class ExtendedStack<T>
	{
		private readonly List<T> _items = new List<T>();

		public List<T> ListItems => _items;
		public int Count => _items.Count;

		public void Push(T item)
		{
			_items.Add(item);
		}

		public T this[int index]
		{
			get => _items[index];
			set => _items[index] = value;
		}

		public T Peek()
		{
			if (_items.Count <= 0) return default(T);

			return _items[_items.Count - 1];
		}

		public T Find(Predicate<T> match)
		{
			return _items.Find(match);
		}

		public bool Contains(T item)
		{
			return _items.Contains(item);
		}

		public T Pop()
		{
			if (_items.Count <= 0) return default(T);

			T temp = _items[_items.Count - 1];
			_items.RemoveAt(_items.Count - 1);
			return temp;
		}

		public void RemoveAt(int itemAtPosition)
		{
			_items.RemoveAt(itemAtPosition);
		}

		public bool Remove(T itemToRemove)
		{
			return _items.Remove(itemToRemove);
		}

		public void PushUp(T item)
		{
			int itemIndex = _items.IndexOf(item);
			for (int i = itemIndex; i < _items.Count - 1; i++)
			{
				Swap(i, i + 1);
			}
		}

		public void PushDown(T item)
		{
			int itemIndex = _items.IndexOf(item);
			for (int i = itemIndex; i >= 1; i--)
			{
				Swap(i, i - 1);
			}
		}

		public List<T> ConvertToList()
		{
			return new List<T>(_items);
		}

		private void Swap(int fromIndex, int toIndex)
		{
			(_items[fromIndex], _items[toIndex]) = (_items[toIndex], _items[fromIndex]);
		}

		public void Clear()
		{
			_items.Clear();
		}
	}
}