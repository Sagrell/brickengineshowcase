﻿using System.Collections.Generic;

namespace BrickEngine
{
    public static class BrickUtils
    {
        public static void Fill<T>(this List<T> list, T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                list.Add(array[i]);
            }
        }
    }
}