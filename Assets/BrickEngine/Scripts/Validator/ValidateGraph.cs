﻿using System;
using System.Collections.Generic;

namespace BrickEngine.Validator
{
    public class ValidateGraph
    {
        private readonly HashSet<Type> _visited = new HashSet<Type>();
        private readonly HashSet<Type> _cycle = new HashSet<Type>();

        public bool IsVisited(Type type)
        {
            return _visited.Contains(type);
        }
        
        public bool IsCycle(Type type)
        {
            return _cycle.Contains(type);
        }
        
        public void Visit(Type type)
        {
            _visited.Add(type);
        }

        public void AddCycle(Type cycle)
        {
            _cycle.Add(cycle);
        }
        
        public void RemoveCycle(Type cycle)
        {
            _cycle.Remove(cycle);
        }
    }
}