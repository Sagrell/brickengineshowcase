﻿using UnityEngine;

namespace BrickEngine.Validator
{
    public class ValidateErrorHandler : IValidateErrorHandler
    {
        public int ErrorCount { get; private set; }

        public void HandleError(string error)
        {
            Debug.LogError(error);
            ErrorCount++;
        }
    }
}