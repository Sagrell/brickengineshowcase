﻿using System;
using System.Collections.Generic;
using GameModule;

namespace BrickEngine.Validator
{
    public class ConfigDependencyValidator
	{
		private readonly Dictionary<EEnvironmentType, BrickEnvironment> _allEnvironments;
		private readonly List<StartupEnvironmentConfig>                 _environments;
		private readonly IValidateErrorHandler                          _errorHandler;
		private readonly bool                                           _validateCollections;

		public ConfigDependencyValidator(List<StartupEnvironmentConfig> environments, IValidateErrorHandler errorHandler, bool validateCollections)
		{
			_environments = environments;
			_errorHandler = errorHandler;
			_validateCollections = validateCollections;
			_allEnvironments = new Dictionary<EEnvironmentType, BrickEnvironment>();
			List<Type> allEnvironmentTypes = AssemblyScanner.GetAllTypes<BrickEnvironment>();
			for (int i = 0; i < allEnvironmentTypes.Count; i++)
			{
				Type environmentType = allEnvironmentTypes[i];
				BrickEnvironment environment = (BrickEnvironment)Activator.CreateInstance(environmentType);
				_allEnvironments[environment.EnvironmentType] = environment;
			}
		}
		
		public void Validate()
        {
			for (int i = 0; i < _environments.Count; i++)
            {
                StartupEnvironmentConfig environmentConfig = _environments[i];
                var dependencyContainer = new DependencyContainer(null, null);
                dependencyContainer.Register<IDependencyResolver>(dependencyContainer, true);
				ValidateGraph graph = new ValidateGraph();
                ValidateEnvironment(environmentConfig, dependencyContainer, graph);
            }
        }

        private void ValidateEnvironment(StartupEnvironmentConfig environmentConfig, DependencyContainer dependencyContainer, ValidateGraph graph)
        {
            BrickEnvironment environment = _allEnvironments[environmentConfig.Type];
            if (environmentConfig.Required.Count > 0)
            {
                for (int requiredIndex = 0; requiredIndex < environmentConfig.Required.Count; requiredIndex++)
                {
                    BrickEnvironment currentEnvironment = _allEnvironments[environmentConfig.Required[requiredIndex]];
					ValidateEnvironment(GetConfig(currentEnvironment.EnvironmentType), dependencyContainer, graph);
                }
            }
            environment.Validate(dependencyContainer, graph, _errorHandler, _validateCollections);
        }

		private StartupEnvironmentConfig GetConfig(EEnvironmentType type)
		{
			for (int i = 0; i < _environments.Count; i++)
			{
				StartupEnvironmentConfig currentEnvironment = _environments[i];
				if (currentEnvironment.Type == type)
				{
					return currentEnvironment;
				}
			}

			return null;
		}
    }
}