﻿namespace BrickEngine.Validator
{
    public interface IValidateErrorHandler
    {
        int ErrorCount { get; }
        void HandleError(string error);
    }
}