﻿using System;
using System.Collections.Generic;
using BrickEngine.Validator;
using GameModule;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BrickEngine
{
    [CreateAssetMenu(fileName = "StartupConfig", menuName = "Settings/StartupConfig")]
    public class StartupConfig : ScriptableObject
    {
        public EEnvironmentType StartEnvironment;
        public List<StartupEnvironmentConfig> Environments;
        
        [Button]
        public void Validate(bool validateCollections)
        {
	        try
	        {
		        IValidateErrorHandler errorHandler = new ValidateErrorHandler();
		        ConfigDependencyValidator validator = new ConfigDependencyValidator(Environments, errorHandler, validateCollections);
		        validator.Validate();
		        if (errorHandler.ErrorCount > 0)
		        {
			        Debug.LogError($"{name}: <color=red>There are some errors!</color> Count: {errorHandler.ErrorCount}");
		        }
	        }
	        catch (Exception e)
	        {
		        Debug.LogError($"{name}: <color=red>Can't resolve dependencies!</color> {e.Message} {e.StackTrace}");
	        }
        }
    }

    [Serializable]
    public class StartupEnvironmentConfig
    {
        public EEnvironmentType Type;
        public List<EEnvironmentType> Required;
    }
}