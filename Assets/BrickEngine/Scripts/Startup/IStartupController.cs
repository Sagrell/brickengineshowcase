namespace BrickEngine
{
	public interface IStartupController
	{
		void Load();
	}
}