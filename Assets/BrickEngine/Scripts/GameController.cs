using System;
using BrickEngine;

namespace GameModule
{
	public static class GameController
	{
		public static event Action<EEnvironmentType> OnSwitchEnvironment;
		public static event Action      OnGameReload;

		public static void Clear()
		{
			OnSwitchEnvironment = null;
		}

		public static void SwitchEnvironment(EEnvironmentType environmentType)
		{
			OnSwitchEnvironment?.Invoke(environmentType);
		}

		public static void ReloadGame()
		{
			OnGameReload?.Invoke();
		}

#if UNITY_EDITOR
		public static event Action OnDatabaseLoad;
		public static void LoadDatabase()
		{
			OnDatabaseLoad?.Invoke();
		}
#endif
	}
}